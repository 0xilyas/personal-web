import "./global.css";
import { Navbar } from "./components/molecules/nav";
import { baseUrl } from "./sitemap";
import { ScrollToTop } from "./components/atoms/scroll-to-top";

export const metadata = {
  metadataBase: new URL(baseUrl),
  title: {
    default: "Ilyas Mutlu.",
    template: "%s | Ilyas Mutlu.",
  },
  description:
    "I like to introduce myself by sharing my reading, learning, writing, and experiences in certain areas. It may take more time to get to know me this way, but I believe it's the best way to truly understand who I am and what I value.",
  openGraph: {
    title: "ilyas mutlu",
    description:
      "I like to introduce myself by sharing my reading, learning, writing, and experiences in certain areas. It may take more time to get to know me this way, but I believe it's the best way to truly understand who I am and what I value.",
    url: baseUrl,
    siteName: "Ilyas Mutlu",
    locale: "en_US",
    type: "website",
  },
  robots: {
    index: false,
    follow: false,
    googleBot: {
      index: false,
      follow: false,
      "max-video-preview": -1,
      "max-image-preview": "large",
      "max-snippet": -1,
    },
  },
};

const cx = (...classes) => classes.filter(Boolean).join(" ");

export default function RootLayout({ children }) {
  return (
    <html lang="en" className={cx(" text-[#d9d9d9] bg-[#181f25] ")}>
      <body>
        <Navbar />
        <main className=" w-11/12 md:w-4/6 mx-4 mt-8 md:mx-12 max-w-4xl lg:mx-auto flex-auto  flex flex-col px-2 md:px-0 mb-10">
          {children}
        </main>
        <ScrollToTop />
      </body>
    </html>
  );
}
