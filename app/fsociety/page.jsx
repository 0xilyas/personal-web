import FSocietyHero from "app/components/atoms/fsocietyhero";
import { FSocietyPosts } from "app/components/organisms/fsocietyposts";

export const metadata = {
  title: "FSociety | Ilyas Mutlu.",
  description: "Read my Writings on Society.",
};

export default function FSocietyPage() {
  return (
    <div>
      <FSocietyHero
        title={"FSociety Posts Section."}
        text={
          "In this section, I will share some of my thoughts on the positive and negative aspects of society."
        }
      />
      <FSocietyPosts />
    </div>
  );
}
