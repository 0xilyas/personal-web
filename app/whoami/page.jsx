export default function AboutMe() {
  return (
    <div>
      <h2>Living Today</h2>
      <p>
        There is only today exists yesterday is just a memory, tomorrow just a
        vision. Each of us is gifted with 24 hours in a day we cannot spend what
        is left in yesterday or borrow it from tomorrow. We choose how to spend
        it. Each moment is an opportunity to shape our path, to take the lessons
        of yesterday and the dreams of tomorrow and transform them into action
        in the present. It is here, in this very moment, that we hold the power
        to create the life we desire.
      </p>
      <br />

      <h2>Life Long Learning</h2>
      <p>
        I was a foolish man, unaware of the true power of knowledge. We are a
        thinking reed, capable of creating our true selves rather than finding
        it. In this journey of life, there is an incredible depth of wisdom in
        the past, a treasure we continue to uncover as we learn and grow until
        our last breath.
      </p>
    </div>
  );
}
