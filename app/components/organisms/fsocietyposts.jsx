import Link from "next/link";
import { formatDate, getPosts } from "app/fsociety/utils";

export function FSocietyPosts() {
  let allBlogs = getPosts();

  return (
    <div className="">
      {allBlogs
        .sort((a, b) => {
          if (
            new Date(a.metadata.publishedAt) > new Date(b.metadata.publishedAt)
          ) {
            return -1;
          }
          return 1;
        })
        .map((thought) => (
          <Link
            key={thought.slug}
            className="  mb-4  "
            href={`/fsociety/${thought.slug}`}
          >
            <div className="bg-[#181e22] mt-5 md:mt-10 mb-6 rounded-xl w-full border-dashed p-5 hover:bg-[#181b1e] mb-4  items-center justify-center border border-dashed border-[#4d94ff]">
              <div className="mb-3 font-bold">
                <h2 className="text-2xl">{thought.metadata.title}</h2>
              </div>

              <small className="font-light text-sm tabular-nums  ">
                {formatDate(thought.metadata.publishedAt, false)}
              </small>
              <div className=" text-neutral-200">
                <p>{thought.metadata.summary}</p>
              </div>
            </div>
          </Link>
        ))}
    </div>
  );
}
