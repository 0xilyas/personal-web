import Link from "next/link";
import { formatDate, getBlogPosts } from "app/blog/utils";

export function BlogPosts() {
  let allBlogs = getBlogPosts();

  return (
    <div className="">
      {allBlogs
        .sort((a, b) => {
          if (
            new Date(a.metadata.publishedAt) > new Date(b.metadata.publishedAt)
          ) {
            return -1;
          }
          return 1;
        })
        .map((post) => (
          <Link
            key={post.slug}
            className="  mb-4  "
            href={`/blog/${post.slug}`}
          >
            <div className="bg-[#181e22] mt-4 mb-6 rounded-xl w-full border-dashed p-5 hover:bg-[#181b1e] mb-4  items-center justify-center border border-dashed border-[#fb2576]">
              <div className="mb-3 font-bold">
                <h2 className="text-2xl">{post.metadata.title}</h2>
              </div>

              <small className="font-light text-sm tabular-nums  ">
                {formatDate(post.metadata.publishedAt, false)}
              </small>
              <div className=" text-neutral-200">
                <p>{post.metadata.summary}</p>
              </div>
            </div>
          </Link>
        ))}
    </div>
  );
}
