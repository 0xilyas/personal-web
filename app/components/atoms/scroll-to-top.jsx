"use client";

import { useEffect, useState } from "react";
import Image from "next/image";

export function ScrollToTop() {
  const [isVisible, setIsVisible] = useState(false);

  const handleScroll = () => {
    if (window.scrollY > 100) {
      setIsVisible(true);
    } else {
      setIsVisible(false);
    }
  };

  const scrollToTop = () => {
    window.scrollTo({
      top: 0,
      behavior: "smooth",
    });
  };

  useEffect(() => {
    window.addEventListener("scroll", handleScroll);
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  return (
    <div>
      {isVisible && (
        <button
          onClick={scrollToTop}
          className="fixed bottom-16 right-5 md:right-72 bg-[#181b1e] text-white rounded-full p-3 shadow-lg transition-transform transform hover:scale-110 transition-all duration-300"
          aria-label="Scroll to top"
        >
          <img
            src="/assets/icons/rocket.png"
            alt="Scroll to top"
            width={32}
            height={32}
            className="object-contain"
          />
        </button>
      )}
    </div>
  );
}
