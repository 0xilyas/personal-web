export default function FSocietyHero({ text, title }) {
  return (
    <section className=" mx-auto">
      <h1 className="mb-4 text-2xl font-bold md:text-8xl md:leading-tight md:font-extrabold">
        {title}
      </h1>
      <p className="mb-3 text-xl md:text-xl text-gray-400 font-bold">{text}</p>
    </section>
  );
}
