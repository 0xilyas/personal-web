"use client";

import { useState } from "react";
import Link from "next/link";

const navItems = {
  "/blog": {
    name: "Blog",
  },
  "/fsociety": {
    name: "FSociety",
  },
  "/whoami": {
    name: "About Me",
  },
};

export function Navbar() {
  const [isOpen, setIsOpen] = useState(false);

  const toggleMenu = () => {
    setIsOpen(!isOpen);
  };

  const handleLinkClick = () => {
    setIsOpen(false);
  };

  return (
    <nav className=" m-5 left-0 border right-0 bg-[#181b1e] p-3 rounded-md shadow-lg z-50">
      <div className="grid grid-cols-2 items-center">
        <div>
          <Link className="font-extrabold text-2xl" href="/">
            Ilyas Mutlu.
          </Link>
        </div>
        <div className="flex justify-end md:hidden">
          <button
            onClick={toggleMenu}
            aria-expanded={isOpen}
            aria-label={isOpen ? "Close menu" : "Open menu"}
            className="text-white focus:outline-none"
          >
            {isOpen ? "❌" : "🍔"}
          </button>
        </div>
        <div className="hidden md:flex justify-end space-x-4">
          {Object.entries(navItems).map(([path, { name }]) => {
            return (
              <Link
                key={path}
                href={path}
                className="hover:text-white flex align-middle relative py-1 px-2 transition-colors duration-200"
              >
                {name}
              </Link>
            );
          })}
        </div>
      </div>
      <div
        className={`md:hidden transition-all duration-300 ease-in-out ${isOpen ? "max-h-screen opacity-100" : "max-h-0 opacity-0 overflow-hidden"}`}
      >
        <div className="flex flex-col space-y-2 mt-2">
          {Object.entries(navItems).map(([path, { name }]) => {
            return (
              <Link
                key={path}
                href={path}
                onClick={handleLinkClick}
                className="hover:text-white flex align-middle relative py-1 px-2 transition-colors duration-200"
              >
                {name}
              </Link>
            );
          })}
        </div>
      </div>
    </nav>
  );
}
