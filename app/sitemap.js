import { getBlogPosts } from "app/blog/utils";
import { getPosts } from "app/fsociety/utils";
export const baseUrl = "https://ilyasmutlu.com";

export default async function sitemap() {
  let blogs = getBlogPosts().map((post) => ({
    url: `${baseUrl}/blog/${post.slug}`,
    lastModified: post.metadata.publishedAt,
  }));

  let fposts = getPosts().map((post) => ({
    url: `${baseUrl}/fsociety/${post.slug}`,
    lastModified: post.metadata.publishedAt,
  }));

  let routes = ["", "/blog"].map((route) => ({
    url: `${baseUrl}${route}`,
    lastModified: new Date().toISOString().split("T")[0],
  }));

  return [...routes, ...blogs, ...fposts];
}
