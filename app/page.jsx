import { BlogPosts } from "app/components/organisms/posts";

export default function Page() {
  return (
    <div>
      <BlogPosts />
    </div>
  );
}
