import { BlogPosts } from 'app/components/organisms/posts'

export const metadata = {
  title: 'Ilyas Mutlu.',
  description: 'Read my Writings.',
}

export default function Page() {
  return (
      <BlogPosts />
  )
}
