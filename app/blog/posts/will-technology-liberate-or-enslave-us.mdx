---
title: "Will Technology Liberate or Enslave Us?"
publishedAt: '2021-11-01'
summary: "I don’t think either of these. Because technology is a tool. It does not by itself liberate or enslave us. How we use technology determines whether it liberates or enslaves us."
---


I don’t think either of these. Because technology is a tool. It does not by
itself liberate or enslave us. How we use technology determines whether it liberates or enslaves us.


Let’s take autopilot as an example. This technology allows an aircraft to
fly straight and level with no manipulation of the pilot. It allows the pilot
to focus on the bigger picture aspects of flight, such as monitoring the weather,
the aircraft’s trajectory, and systems, rather than dealing with constant checks.

A second automatic system, called fly-by-wire, also prevents airplanes from entering
an aerodynamic stall; this is a dangerous situation that occurs when the nose of an
airplane is tilted upwards at a very steep angle. Thus, autopilot and cable flight
have made flying safer than ever before.

But on May 31, 2009, disaster struck [Air France Flight #447](https://hbr.org/2017/09/the-tragic-crash-of-flight-af447-shows-the-unlikely-but-catastrophic-consequences-of-automation), an aircraft equipped
with autopilot and Fly-by-wire systems.

Disaster struck when a pressure probe outside the plane froze. As a result, the
autopilot system was no longer able to determine how fast the plane was going.
Because this information was not available, the autopilot was disabled. And
the wireline flight system has switched to a different mode that is no longer protected against a stall.

When the autopilot disengaged, the co-pilot put his hand on the control stick
and pulled it back, poking his nose up and sending the plane into a stall.
Warnings sounded in the cockpit, but neither pilot nor co-pilot appeared to
know what had happened. No one made a simple correction by lowering the nose of the airplane to end the stall.

Four minutes and twenty seconds later, Flight 447 crashed into the Atlantic
Ocean, killing all 228 people on board.

Automatic aircraft systems do not require pilots to perform basic maneuvers
and allow them to control wider aspects of their flight. They create an environment that allows safer flying.

On the other hand, these automated systems can make pilots dependent on technology.
It makes it easier for pilots to forget or ignore basic flying skills, further
increasing their dependence on autopilot. But when the autopilot fails or
shuts down, such human pilots may not know how to respond.

This kind of technological dependency could be the cause of the Flight 447
disaster. Perhaps the pilots did not take appropriate action to avoid stalls
because they were so accustomed to airplanes that prevented stalls that they
did not know how to get out of this serious situation on their own.

Of course, autopilot and fly-by-wire were not responsible for Flight 447 crash.
They didn’t cause stalls or prevent pilots from towing the plane. Humans have
created a situation where an otherwise safe technology has contributed to a terrible tragedy.

Examples of the liberating and enslaving power of technology are everywhere.

Modern smartphones allow us to access information and communicate with friends,
family and emergency personnel from almost anywhere in the world. In addition,
they allow us to browse for hours on media such as social media.

Splitting the atom was a huge step forward in human history and offered the
promise of a powerful energy source. It also led to the creation of weapons
that could instantly wipe out other people’s entire populations, resulting in an escalating arms race.

The point is, technology, whether it’s autopilot, smartphones, or nuclear
power, does not liberate or enslave us on our own. The way we use technology
determines its impact on our lives. If we use it carefully and purposefully,
it can free us to be more creative, productive, and caring. But if we use it
ignorantly and haphazardly, we allow ourselves to become dependent on technology
and risk neglecting other important behaviors.
